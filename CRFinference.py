import cv2
import numpy as np
import tensorflow as tf
import utils as utils

# try importing CRF, ow install it
try:
	import pydensecrf.densecrf as dcrf
except ImportError:
	print("please install pydensecrf package by running \n pip install git+https://github.com/lucasb-eyer/pydensecrf.git")
from pydensecrf.utils import compute_unary, create_pairwise_bilateral, create_pairwise_gaussian, unary_from_softmax

# sample input test file
filename= 'data/inria_aerial_images/AerialImageDataset/train/images/austin1.tif'
gt_name = 'data/inria_aerial_images/AerialImageDataset/train/gt/austin1.tif'
original_image = cv2.imread(filename,1)
#cv2.resize(input_image, (500,500))

# logs folder and model file
logs_dir = 'logs/'
meta_file = logs_dir+'model.cpkt-49999.meta'

sess = tf.Session()

saver = tf.train.import_meta_graph(meta_file)
saver.restore(sess,tf.train.latest_checkpoint(logs_dir))

graph = tf.get_default_graph()
image_place = graph.get_tensor_by_name("input_image:0")
label_place = graph.get_tensor_by_name("label:0")
keep_probability = graph.get_tensor_by_name("keep_probabilty:0")


#pred_annotation = graph.get_tensor_by_name("prediction:0")

prediction = graph.get_tensor_by_name("prediction:0")
final_layer = graph.get_tensor_by_name("Final_layer:0")
probabilities = tf.nn.softmax(final_layer)

accuracy_levels=[]
for x_offset in range(0,5000, 1000):
	for y_offset in range(0,5000, 1000):
		
		input_image = original_image[0+x_offset:1000+x_offset, 0+y_offset:1000+y_offset, :]

		input_image = np.array(input_image, np.float32)
		gt_image = cv2.imread(gt_name,0)[0+x_offset:1000+x_offset, 0+y_offset:1000+y_offset]

		tensor_image = np.expand_dims(input_image, 0)
	
		preds, probs = sess.run([prediction, probabilities], feed_dict={image_place: tensor_image, keep_probability: 1.0})

		softmax = probs.squeeze()
		softmax = softmax.transpose((2,0,1))

		## get the nagative log likelihood for the softmax
		unary = unary_from_softmax(softmax)
		unary = np.ascontiguousarray(unary)

		CRF = dcrf.DenseCRF(input_image.shape[0] * input_image.shape[1], 2)

		CRF.setUnaryEnergy(unary)

		# This potential penalizes small pieces of segmentation that are
		# spatially isolated -- enforces more spatially consistent segmentations
		feats = create_pairwise_gaussian(sdims=(10, 10), shape=input_image.shape[:2])
		CRF.addPairwiseEnergy(feats, compat=3, kernel=dcrf.DIAG_KERNEL, normalization=dcrf.NORMALIZE_SYMMETRIC)

		# This creates the color-dependent features --
		# because the segmentation that we get from CNN are too coarse
		# and we can use local color features to refine them
		feats = create_pairwise_bilateral(sdims=(50, 50), schan=(20, 20, 20),
		                                   img=input_image, chdim=2)

		CRF.addPairwiseEnergy(feats, compat=10,
		                     kernel=dcrf.DIAG_KERNEL,
		                     normalization=dcrf.NORMALIZE_SYMMETRIC)
		Q = CRF.inference(5)
		res = np.argmax(Q, axis=0).reshape((input_image.shape[0], input_image.shape[1]))
		color_coded_res = np.zeros(shape=(500,500,3), dtype=np.uint8)
		color_coded_res[res==1] = [0,0,255]

		unary_out = preds.squeeze()
		color_coded_unary_out = np.zeros(shape=(500,500,3), dtype=np.uint8)
		color_coded_unary_out[unary_out==1]=[0,0,255]

		
		gt_color =  np.zeros(shape=(500,500,3), dtype=np.uint8)
		gt_color[gt_image==255]=[0,0,255]
		gt_over =  np.uint8(input_image/2.0 + gt_color/2.0)


		overlaid_res = np.uint8(input_image/2.0 + color_coded_unary_out/2.0)

		grand_out = np.concatenate([np.uint8(input_image), overlaid_res, gt_over], axis=1)
		#cv2.imshow("testing", grand_out)
		#cv2.imwrite("testing.png", grand_out)
		#cv2.waitKey(0)

		## now compute the accuracy
		acc = utils.performance_measures(res, gt_image/255)
		print(acc)
		accuracy_levels.append(acc)

print(np.mean(accuracy_levels))




