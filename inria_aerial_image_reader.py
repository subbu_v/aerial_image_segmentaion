import numpy as np
import scipy
import scipy.misc as misc
import random
import os
import zipfile
import cv2

INRIA_DICT = {
	## data augmentation options
	"random_resize":True,
	"resize_chance":0.4,
	"lower_size" :0.4,
	"higher_size":1.7,
	"sigma"      :0.15,

	"random_crop":True,
	"crop_chance":0.8,
	"patch_height":1000,
	"patch_width":1000
}



## some utility functions
def may_be_download_data(dest_folder):
	data_url = "https://files.inria.fr/aerialimagelabeling/AerialImageDataset.zip" 
	data_zip = dest_folder+"AerialImageDataset.zip"
	if not os.path.exists(data_zip):
		print("dataset is not found,, downloading it -------------------->")
		os.system("wget "+data_url+" -P "+"data/inria_aerial_images/ --no-check-certificate")

	## extract the data
	if not os.path.exists(dest_folder+"AerialImageDataset/"):
		zipfile.ZipFile(data_zip, 'r').extractall(dest_folder)

	data_dir = dest_folder+"AerialImageDataset/"
	## prepate text files
	trainval_txt = "trainval.txt"
	test_txt = "test.txt"
	val_txt = "val.txt"
	train_txt = "train.txt"

	if not os.path.exists(data_dir+trainval_txt):
		print("train.txt is not found, so creating ------->")
		with open(data_dir+trainval_txt, "w") as f:
			training_img_folder = "train/images/"
			training_gt_folder = "train/gt/"
			for item in os.listdir(data_dir+training_img_folder):
				f.write(training_img_folder+item+" "+training_gt_folder+item+"\n")

	if not os.path.exists(data_dir+test_txt):
		print("test.txt is not found, so creating ------->")
		with open(data_dir+test_txt, "w") as f:
			testing_img_folder = "test/images/"
			for item in os.listdir(data_dir+testing_img_folder):
				f.write(testing_img_folder+item+"\n")

	if not os.path.exists(data_dir+val_txt):
		print("val.txt is not found, so splitting trainval---------->")
		trainval_list =  open(data_dir+trainval_txt, "r").readlines()
		num_images = len(trainval_list)
		train_list = trainval_list[0:num_images-5]
		val_list = trainval_list[num_images-5:-1]
		with open(data_dir+train_txt, "w") as f:
			for item in train_list:
				f.write(item)
		with open(data_dir+val_txt, "w") as f:
			for item in val_list:
				f.write(item)

class ImageBatchReader:
	batch_offset = 0
	epochs_completed = 0
	DATA_AUGMENT = False

	def __init__(self, data_folder, data_list, input_size=(1000,1000), train=True):
		
		self.input_size = input_size
		self.train = train
		# if training, activate data augmentation processes
		if self.train:
			self.DATA_AUGMENT = True
		# check if data is not there
		may_be_download_data(dest_folder=data_folder)
		self.data_folder = data_folder+'AerialImageDataset/'
		self.data_list = self.data_folder+data_list
		
		self.image_list = []
		self.label_list = []

		with open(self.data_list, 'r') as f:
			for line in f:
				img, label = line.rstrip('\n').split(' ')
				self.image_list.append(self.data_folder+img)
				self.label_list.append(self.data_folder+label)

		self.image_list = np.array(self.image_list)
		self.label_list = np.array(self.label_list)

		if self.train:
			### shuffle
			perm = np.arange(len(self.image_list))
			np.random.shuffle(perm)
			self.image_list = self.image_list[perm]
			self.label_list = self.label_list[perm]

	def next_batch(self, batch_size=1):
		start = self.batch_offset
		self.batch_offset += batch_size

		if self.batch_offset > len(self.image_list):
			## finished epoch
			self.epochs_completed +=1
			print("****************** Epochs completed: " + str(self.epochs_completed) + "******************")
			if self.train:
				# Shuffle the data
				perm = np.arange(len(self.image_list))
				np.random.shuffle(perm)
				self.image_list = self.image_list[perm]
				self.label_list = self.label_list[perm]
			# Start next epoch
			start = 0
			self.batch_offset = batch_size

		end = self.batch_offset
		return self.return_the_batch(start, end)
	
	def return_the_batch(self, start, end):
		images = []
		labels = []
		for index in range(start, end):
			img_file = self.image_list[index]
			gt_file = self.label_list[index]
			img = misc.imread(img_file, mode='RGB')
			label =misc.imread(gt_file, mode='RGB')

			if self.DATA_AUGMENT:
				img, label = self.jitter(img, label)
			label=self.color2class(label)
			images.append(img)
			labels.append(label)

		return np.array(images), np.expand_dims(np.array(labels), axis=3)

	def color2class(self, img):
		gt = np.zeros((img.shape[0], img.shape[1]), dtype=int)  # only one channel
		road_id = 1
		road_color = 255
		affected_pixels = np.all(img == road_color, axis=2)
		gt += affected_pixels*road_id
		return gt
	
	def class2color(self, class_img): # class 1 assumed to be foreground
		color_img = np.zeros((class_img.shape[0], class_img.shape[1], 3), dtype=np.uint8)
		road_id = 1
		road_color = [0,0,255]
		color_img[class_img==road_id]=road_color
		return color_img
	
	def reset_batch_offset(self):
		self.batch_offset = 0

	### TODO implement some more data augmentation methods like random rotation

	def jitter(self, img, label):

		### random resize
		random_resize = INRIA_DICT["random_resize"]
		resize_chance = INRIA_DICT["resize_chance"]
		if random_resize and resize_chance > random.random():
			lower_size = INRIA_DICT["lower_size"]
			higher_size = INRIA_DICT["higher_size"]
			sigma = INRIA_DICT["sigma"]

			factor = random.normalvariate(1, sigma)
			if factor < lower_size: 
				factor = lower_size
			if factor > higher_size:
				factor = higher_size
			img = misc.imresize(img, factor)
			label = misc.imresize(label, factor, interp='nearest')

		### random crop
		random_crop = INRIA_DICT["random_crop"]
		crop_chance = INRIA_DICT["crop_chance"]
		height      = INRIA_DICT["patch_height"]
		width       = INRIA_DICT["patch_width"]
		if random_crop and crop_chance > random.random():
			old_height = img.shape[0]
			old_width = img.shape[1]
			max_c = max(old_width - width, 0) 
			max_r = max(old_height - height, 0)
			offset_r = random.randint(0, max_r)
			offset_c = random.randint(0, max_c)
			img = img[offset_r:offset_r+height, offset_c:offset_c+width]
			label = label[offset_r:offset_r+height, offset_c:offset_c+width]

		### make sure image size is required input size
		if not img.shape[0] == self.input_size[0]:
			img = misc.imresize(img, self.input_size)
			label = misc.imresize(label, self.input_size, interp='nearest')
		return img, label



if __name__=='__main__':
	reader = ImageBatchReader(data_folder='data/inria_aerial_images/', data_list='train.txt', train=True)

	image_batch, label_batch = reader.next_batch(batch_size=1)
	label_class_img = label_batch[0,:,:,0]
	label_color_img = reader.class2color(class_img=label_class_img)
	cv2.imwrite('aerial.png',np.concatenate([image_batch[0], label_color_img], axis=1))
	os.system('eog aerial.png')



